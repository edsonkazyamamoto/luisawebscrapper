import uuid
from datetime import datetime, timedelta
from flask import jsonify, abort, request, Blueprint, render_template

from service.cifraclubService import cifraclubService
import threading
import logging
import json
from multiprocessing.pool import ThreadPool
from flask import jsonify
from flask import Response
from flask import send_file
from flask import jsonify
import multiprocessing
from multiprocessing import Process, Value
import json
from bson import ObjectId
import util
from os import listdir
from os.path import isfile, join

service = cifraclubService()
REQUEST_API = Blueprint('request_Music', __name__, url_prefix='/music/')

config = util.getConfig()
def get_blueprint():
    """Return the blueprint for the main app module"""
    return REQUEST_API

@REQUEST_API.route('', methods=['GET'])
def get_queue_records():
    """
    Retorna todas as musicas que serão buscadas
    ---
    tags:
      - Music controller
    responses:
        200:
            description: retorna as companias em forma de JSON
    """
    pool = ThreadPool(processes=1)
    async_result = pool.apply_async(service.getFila, ())

    return jsonify(async_result.get()), 200

@REQUEST_API.route('<string:_id>', methods=['GET'])
def record_a_unique_music_into_queue(_id):
    """
    Método que adiciona musicas para serem buscadas na fila
    ---
    tags:
      - Music controller
    parameters:
      - in: path
        name: _id
        description: chave
        required: true
        type: string
    responses:
        200:
            description: adiciona a empresa para busca na fila
    """
    if _id == "":
        abort(404)

    pool = ThreadPool(processes=1)
    async_result = pool.apply_async(service.adicionarFila, args=[_id])
    async_result.get()

    return _id, 200

@REQUEST_API.route('service/run', methods=['GET'])
def run_web_scrapper():
    """
    Executa todos os servicos enfileirados
    ---
    tags:
      - Music controller
    responses:
        200:
            description: executa assincronamente o crawler de busca.
        """

    p1 = Process(target=service.executarScrapper, args=())
    p1.start()

    return "executing", 200

@REQUEST_API.route('service/getAllMusic', methods=['GET'])
def getAllMusics():
    """
    Retorna todas as musicas
    ---
    tags:
      - Music controller
    responses:

        200:
            description: retorna todas as musicas que ja foram pesquisadas
    """

    onlyfiles = [open(config["FileRepository"]+"/"+f, "r").readlines() for f in listdir(config["FileRepository"]) if isfile(join(config["FileRepository"], f))]

    return jsonify(onlyfiles), 200

@REQUEST_API.route('service/get/<string:_id>', methods=['GET'])
def getAUniqueMusic(_id):
    """
    Retorna apenas uma unica musica
    ---
    tags:
      - Music controller
    parameters:
      - in: path
        name: _id
        description: chave
        required: true
        type: string
    responses:

        200:
            description: retorna a musica passada pelo ID
    """
    file = ""
    if _id == "":
        abort(404)
    try:
        file = open(config["FileRepository"]+"/"+_id+".txt", "r").readlines()
    except Exception:
        print(Exception)

    return jsonify(file), 200
