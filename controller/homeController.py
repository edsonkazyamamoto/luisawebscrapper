"""The Endpoints to manage the BOOK_REQUESTS"""
import uuid
from datetime import datetime, timedelta
from flask import jsonify, abort, request, Blueprint, render_template
import threading
import logging
import json
from multiprocessing.pool import ThreadPool
from flask import jsonify
from flask import Response
from flask import send_file
from flask import jsonify
import multiprocessing
from multiprocessing import Process, Value
import json
from bson import ObjectId
import util
REQUEST_API = Blueprint('request_home', __name__, url_prefix='/')

config = util.getConfig()
def get_blueprint():
    """Return the blueprint for the main app module"""
    return REQUEST_API

@REQUEST_API.route('/', methods=['GET'])
def home():
    """
    Pagina inicial da aplicacao
    ---
    responses: 
        200: 
            description: faz executar assincronamente o crawler de busca.
    """
    return render_template('index.html')

@REQUEST_API.route('/form/<string:_code>', methods=['GET'])
def form(_code):
    """
    Return all book requests
    ---
    parameters:
      - in: path
        name: _code
        description: codigo
        required: true
        type: string
    responses:
        200:
            description: faz executar assincronamente o crawler de busca.
    """
    if _code != "":
        print(_code)

    return jsonify(mongo.Mongo().GetCodeList("ovos",  _code)),200
