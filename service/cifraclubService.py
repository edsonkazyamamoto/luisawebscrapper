from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import util
import threading, queue

import logging
import requests

config = util.getConfig()
class cifraclubService(object):
    def __init__(self):
        print("inicio do servico")
        self.fila = queue.Queue()
        self.filaProcessada = queue.Queue()

    def adicionarFila(self, fila):
        self.fila.put(fila)

    def getFila(self):
        return list(self.fila.queue)

    def executarScrapper(self):
        threading.Thread(target=self.__worker__, daemon=True).start()
        self.fila.join()

        threading.Thread(target=self.__sendProcessedCompany__, daemon=True).start()
        self.filaProcessada.join()

    def __sendProcessedCompany__(self):
        while True:
            processados = self.filaProcessada.get()
            r = requests.post("/reports",
                              json={'nome': processados['nome'],
                                    'cifra': processados['cifra']})

            self.filaProcessada.task_done()

    def __worker__(self):
        while True:
            musica = self.fila.get()
            logging.info(f'Working on {musica}')
            driver = webdriver.Firefox(executable_path=r'./geckodriver')
            driver.get(f"https://www.cifraclub.com.br/?q="+musica)

            delay = 10  # seconds
            try:
                webWaiter = WebDriverWait(driver, delay)

                allSearch = webWaiter.until(EC.presence_of_element_located((By.XPATH, "//*[@id=\"___gcse_0\"]/div/div/div/div[5]/div[2]/div/div/div[1]")))
                historyList = allSearch.find_elements_by_xpath(".//div")
                for itemHistory in historyList:
                    try:
                        # cada item do navegador
                        print(itemHistory
                              .find_element_by_xpath(".//div[1]/div[1]/div[1]/a").text)
                        itemHistory\
                            .find_element_by_xpath(".//div[1]/div[1]/div[1]/a")\
                            .click()
                        break
                    except Exception:
                        Exception

                texto = webWaiter.until(EC.presence_of_element_located((By.XPATH, "//*[@id=\"js-w-content\"]/div[3]/div[1]/div[1]/div[1]/div/div/pre")))

                f = open(f"{config['FileRepository']}/{musica}.txt", "a")
                f.write(texto.text)
                f.close()

                self.filaProcessada.put({
                    'nome': musica,
                    'cifra': texto.text
                })

            except Exception as e:
                logging.error(str(e))
            finally:
                driver.quit()

            self.fila.task_done()