import argparse
import os
from flask import Flask, jsonify, make_response
from flask_cors import CORS
from flask_swagger import swagger
from flask_swagger_ui import get_swaggerui_blueprint
from controller import homeController
from controller import musicController
import logging
import util
from flask import send_from_directory

config = util.getConfig()

APP = Flask(__name__, template_folder='templates')

### swagger specific ###
SWAGGER_URL = '/swagger'
API_URL = '/spec'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Music Crawller"
    }
)

@APP.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('./templates/js', path)

@APP.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('./templates/css', path)

APP.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
### end swagger specific ###

logging.basicConfig(filename='demo.log', level=logging.DEBUG)
APP.register_blueprint(musicController.get_blueprint())
APP.register_blueprint(homeController.get_blueprint())

@APP.route("/spec")
def spec():
    swag = swagger(APP)
    swag['info']['version'] = "1.0"
    swag['info']['title'] = "Crawller Reclame Aqui"
    return jsonify(swag)

@APP.errorhandler(400)
def handle_400_error(_error):
    """Return a http 400 error to client"""
    return make_response(jsonify({'error': 'Misunderstood'}), 400)

@APP.errorhandler(401)
def handle_401_error(_error):
    """Return a http 401 error to client"""
    return make_response(jsonify({'error': 'Unauthorised'}), 401)

@APP.errorhandler(404)
def handle_404_error(_error):
    """Return a http 404 error to client"""
    return make_response(jsonify({'error': 'Not found'}), 404)


@APP.errorhandler(500)
def handle_500_error(_error):
    """Return a http 500 error to client"""
    return make_response(jsonify({'error': 'Server error'}), 500)


if __name__ == '__main__':

    PARSER = argparse.ArgumentParser( description="Seans-Python-Flask-REST-Boilerplate")

    PARSER.add_argument('--debug', action='store_true', help="Use flask debug/dev mode with file change reloading")
    ARGS = PARSER.parse_args()

    PORT = int(os.environ.get('PORT', config['Port']))
    print("porta:"+str(PORT))
    APP.run(host=config['Host'], port=PORT, debug=True)

    #if ARGS.debug:
    #    print("Running in debug mode")
    #    CORS = CORS(APP)
    #    APP.run(host=config['Host'], port=PORT, debug=True)
    #else:
    #    APP.run(host=config['Host'], port=PORT, ssl_context=('cert.pem', 'key.pem'), debug=False)