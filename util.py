import json

from bson import ObjectId
def getConfig():
    with open('config.json') as json_data_file:
        return json.load(json_data_file)


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)

